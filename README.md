# 99 Bottles of Beer

Starter project for 99 Bottles of OOP using the song 99 Bottles of Beer on the wall.

## From Appendix A: Initial Exercise
> The test suite contains one failing test, and many skipped tests. Your goal is to write code that passes all of the tests. 

> Follow this protocol:
>  * run the tests and examine the failure
>  * write only enough code to pass the failing test
>  * unskip the next test (this simulates writing it yourself)
> Repeat the above until no test is skipped, and you’ve written code to pass each one.

> #### Work on this task for *30 minutes*. 
> The vast majority of folks do not finish in 30 minutes, but it’s useful, for later comparison purposes, to record how far you got. Even if you can’t force yourself to stop at that point, take a break at 30 minutes and save your code.
